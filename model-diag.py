#Plot model diagram
import tensorflow as tf
import pandas as pd
import numpy as np 
from keras.models import Sequential
from keras.layers import Dense
from keras.models import load_model
import matplotlib.pyplot as plt
import os
from keras.utils.vis_utils import plot_model

model = load_model('/home/dnikic/Downloads/faks/master/vicon-pupil-interaction-intention-classifier/danilopc-resaults/reorg/danilo-e500-b64-25-4-2020/Z2/19.8.2019-m-d/' + '/my_model.h5')
plot_model(model, to_file='model_diagram_Z2.png', show_shapes=True, show_layer_names=True)
del model 
model = load_model('/home/dnikic/Downloads/faks/master/vicon-pupil-interaction-intention-classifier/danilopc-resaults/reorg/danilo-e500-b64-25-4-2020/Z3/19.8.2019-m-d/' + '/my_model.h5')
plot_model(model, to_file='model_diagram_Z3.png', show_shapes=True, show_layer_names=True)


