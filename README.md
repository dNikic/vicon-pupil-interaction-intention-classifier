# vicon-pupil interaction intention classifier

Using machine learning detect weather there is an interaction intention. 

The input dataset is captured using Vicon Motion Capture system and Pupil Capture equipped with RealSens Gaze tracking.

The goal is comparing how well can an interaction intention be detected using vicon+pupil system compared to pupil only.

Sowftware used to collect data:
https://gitlab.com/vicon-pupil-data-parser/vajkonstrim/tree/linuxport

You may use blender csv tools for post processing of data as keyframes located at:
https://github.com/dnikic/Blender-CSV-import-export

Training dataset:
https://gitlab.com/dNikic/vicon-pupil-dataset

#Note
Use "fornetwork" branch of training dataset

#Install
python3 -m venv env
source env/bin/activate
pip3 install -r requirements.txt

#Usage
python3 combine-all-csv.py 
mkdir Z2V1
mkdir Z3V1
python3 batch.py
