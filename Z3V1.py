#trainX, trainy, testX, testy

import tensorflow as tf
import pandas as pd
import numpy as np 
from keras.models import Sequential
from keras.layers import Dense
from keras.models import load_model
from keras.layers import Flatten
from keras.layers import Dropout
from keras.layers import LSTM
from keras.utils import to_categorical
import matplotlib.pyplot as plt
import os


def percentages(input_list):
    unique = set(input_list)
    unique = list(unique)
    occurances = []
    # print('Values percentages:')
    for i in range(0,len(unique)):
        occurance = input_list.count(unique[i])
        occurance = occurance / (len(input_list)/100)
        occurances.append(occurance)
        # print(unique[i],occurances[i],'%')
    return unique,occurances

def reverse_one_hot(testy):
    testy_toclass =[]
    for i in range (0,testy.shape[0]):
        class_nbr = testy[i].tolist().index(1.0)
        testy_toclass.append(class_nbr)
    return testy_toclass

def main(datapath,epochs,batch):

    # load the dataset
    # datapath = 'combined.csv'
    # datapath = '/home/dnikic/Downloads/faks/master/vicon-pupil-dataset/21.8.2019/streamcpp/output/cizla_podignuto' + '/output.csv'
    # datapath = '/home/dnikic/Downloads/faks/master/vicon-pupil-interaction-intention-classifier' + '/pima-indians-diabetes.data.csv'
    df = pd.read_csv(datapath, delimiter=',')
    # random_state = 200 #Random state is a seed value
    #Generate output folder
    out_folder, _ = datapath.rsplit('/', 1)
    out_folder = out_folder.replace('/', '-')
    out_folder = "Z3V1/" +  out_folder 
    try:
        os.makedirs(out_folder)
    except:
        print('\n Folder not created: ',out_folder,'\n')
    #Split 70% for train, 30% for test
    train = df.head(int(len(df)*(70/100)))#Get first 70% of dataset without randomization
    test = df.drop(train.index)
    last_col_name = df.columns[-1]#Last column is used for storing annotations

    #Balance train set
    # unique_in_set = train[last_col_name].unique().tolist()#[1, 0.5, 0]
    # interaction_label = train.index[train[last_col_name] == 1].tolist()#Has interaction label == 1
    # for i in range(0, len(unique_in_set)):
    #     i_class = train.index[train[last_col_name] == i].tolist()
    #     if len(i_class) > len(interaction_label):
    #         diference = len(i_class) - len(interaction_label)
    #         i_class = i_class[:diference]
    #         train = train.drop(i_class)


    #Extract labels and inputs
    trainX = train.drop([last_col_name], axis=1).to_numpy(copy=True) *10 #Train data inputs = numpy array without annotations column.
    trainy = train[last_col_name].to_numpy(copy=True) *10 #Labels for train process = numpy array from annotations column.
    testX = test.drop([last_col_name], axis=1).to_numpy(copy=True  *10)#Test data inputs = numpy array without annotations column.
    testy = test[last_col_name].to_numpy(copy=True) *10 #Labels for testing = numpy array from annotations column.
    # one hot encode y
    # print(testy[150])
    trainy = to_categorical(trainy)
    testy = to_categorical(testy)
    #Reverse one hot encode y
    # testy = reverse_one_hot(testy)
    #Convert 2d array into a 3d array for LSTM
    trainX = np.dstack([trainX]*1)
    testX = np.dstack([testX]*1)
    n_timesteps, n_features, n_outputs = trainX.shape[1], trainX.shape[2], trainy.shape[1]# verbose, epochs, batch_size = 0, 300, 64 #0, 15, 64

    # Define the keras model
    model = Sequential()
    model.add(LSTM(100, input_shape=(n_timesteps,n_features)))
    model.add(Dropout(0.5))
    model.add(Dense(100, activation='relu'))
    model.add(Dense(n_outputs, activation='softmax'))
    # Compile the keras model
    model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
    # Fit the keras model on the dataset
    # epochs = 150
    # batch = 64
    history = model.fit(trainX, trainy, epochs=epochs, batch_size=batch)

    # Plot training values
    plt.plot(history.history['accuracy'])
    plt.title('Model accuracy: '+ str(history.history['accuracy'][-1]))
    plt.ylabel('Accuracy')
    plt.xlabel('Epoch')
    plt.legend(['Train', 'Test'], loc='upper left')
    plt.savefig(out_folder + '/acc.png')
    plt.clf()
    plt.plot(history.history['loss'])
    plt.title('Model loss: ' + str(history.history['loss'][-1]))
    plt.ylabel('Loss')
    plt.xlabel('Epoch')
    plt.legend(['Train', 'Test'], loc='upper left')
    plt.savefig(out_folder + '/loss.png')
    plt.clf()
    #Save, clear and load model
    model.save(out_folder + '/my_model.h5') 
    del model 
    model = load_model(out_folder + '/my_model.h5')

    # Evaluate the model on the test data using `evaluate`
    print('\n# Evaluate on test data')
    results = model.evaluate(testX, testy, batch_size=batch,verbose=0)
    print('test loss, test acc:', results)

    #Prepare outputs for ploting (column to list)
    testy = reverse_one_hot(testy)
    predictions = model.predict_classes(testX, batch_size=batch)
    # predictions = np.concatenate( predictions, axis=0 ).tolist()
    # predictions = [1,1,3,3,5,5,5]
    predictions = [number / 10 for number in predictions]
    testy = [number / 10 for number in testy]
    prec_predictions = [round(num, 2) for num in predictions]
   #Covnert to 0,05 and 1 values
    predictions = [2*x for x in predictions]
    predictions = [round(num, 0) for num in predictions]
    predictions = [0.5*x for x in predictions]

    #Test ploting
    plt.gcf().set_size_inches(18.5, 10.5)#Plot settings
    plt.title('Epochs: '+ str(epochs) + ' Batches: ' + str(batch) + '\n' + 'interaction [1.0], No interaction [0.5], Outlier [0.0]' )
    plt.plot(testy,color='blue',linewidth=6)#Labeled
    plt.plot(predictions,color='red',linewidth=4)#Predicted
    plt.plot(prec_predictions,color='orange',linewidth=2)#Precise predicted
    plt.legend([ 'ground','predict','precise prediction'], loc='upper left')#Legend
    plt.savefig(out_folder + '/test' + '.png')
    plt.clf()
    #Predictions pie chart
    plt.title('Value distrubution \n' +'Epochs: '+ str(epochs) + ' Batches: ' + str(batch) + '\n' + 'interaction [1.0], No interaction [0.5], Outlier [0.0]' )
    unique,occurances = percentages(predictions)
    plt.pie(occurances, labels=unique,
    autopct='%1.1f%%', shadow=True, startangle=140)
    plt.axis('equal')
    plt.savefig(out_folder + '/pie' + '.png')
    plt.clf()

    #Preparation for Confusion matrix
 
    data = {'y_Actual': testy  ,'y_Predicted': predictions }#Convert to dataframe
    df = pd.DataFrame(data, columns=['y_Actual','y_Predicted'])
    #Remove outliers from labels
    df = df[df.y_Actual != 0]
    df = df[df.y_Predicted != 0]
    #Make all predicted values 0 or 1
    df.loc[df['y_Actual'] == 0.5, 'y_Actual'] = 0
    df.loc[df['y_Predicted'] == 0.5, 'y_Predicted'] = 0
    #Confusion matrix plot
    import seaborn as sn
    confusion_matrix = pd.crosstab(df['y_Actual'], df['y_Predicted'], rownames=['Actual'], colnames=['Predicted'], margins = True)
    sn.heatmap(confusion_matrix, annot=True, fmt='g')    
    tp = confusion_matrix.iloc[0][0]
    fn = confusion_matrix.iloc[1][0]
    fp = confusion_matrix.iloc[0][1]
    tn = confusion_matrix.iloc[1][1]
    recall = tp/(tp + fn)
    precision = tp/(tp + fp)
    f_measure = (2*recall * precision)/(recall + precision) 
    plt.title(
        'Confusion matrix \n ' + '\n' 
        'interaction [1.0], No interaction [0.0] \n' 
        'tp: ' + str(tp) + ' '
        'fn: ' + str(fn) + ' '
        'fp: ' + str(fp) + ' '
        'tn: ' + str(tn) + '\n'
        'recall: ' + str(recall) + ' '
        'precision: ' + str(precision ) + ' '
        'f_measure: ' + str(f_measure) + ' '
    )
    plt.ylabel('True label')
    plt.xlabel('Predicted label')  
    plt.savefig(out_folder + '/confusion' + '.png')
    plt.clf()




# datapath = '/home/dnikic/Downloads/faks/master/vicon-pupil-dataset/30.08.2019/streamcpp/30.08TM/podignuto' + '/output.csv'
# batch = 64
# epochs=2#150
# main(datapath,epochs,batch)