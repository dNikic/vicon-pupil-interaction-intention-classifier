import Z2V1
import Z3V1
import os



def find_csv(rootdir):
    # rootdir = '/home/dnikic/Downloads/faks/master/vicon-pupil-dataset'  # Dets test sets
    extensions = ('.csv')
    first = True
    for subdir, dirs, files in os.walk(rootdir):
        for file in files:
            ext = os.path.splitext(file)[-1].lower()
            if ext in extensions:
                file_path = os.path.join(subdir, file)
                if '.git' not in file_path and 'scripts' not in file_path and '30.08TM/srednje/' not in file_path:
                    print(file_path)






datapath = [
    '/home/dnikic/Downloads/faks/master/vicon-pupil-dataset/29.08.2019/streamcpp/29.8/s-m/podignuto/output.csv',
    '/home/dnikic/Downloads/faks/master/vicon-pupil-dataset/29.08.2019/streamcpp/29.8/s-m/spusteno/output.csv',
    '/home/dnikic/Downloads/faks/master/vicon-pupil-dataset/29.08.2019/streamcpp/29.8/s-m/srednje/output.csv',
    '/home/dnikic/Downloads/faks/master/vicon-pupil-dataset/29.08.2019/streamcpp/29.8/a-n/podignuto/output.csv',
    '/home/dnikic/Downloads/faks/master/vicon-pupil-dataset/29.08.2019/streamcpp/29.8/a-n/spusteno/output.csv',
    '/home/dnikic/Downloads/faks/master/vicon-pupil-dataset/29.08.2019/streamcpp/29.8/a-n/sredina/output.csv',
    '/home/dnikic/Downloads/faks/master/vicon-pupil-dataset/21.8.2019/streamcpp/output/cizla_spusteno/output.csv',
    '/home/dnikic/Downloads/faks/master/vicon-pupil-dataset/21.8.2019/streamcpp/output/cizla_srednje/output.csv',
    '/home/dnikic/Downloads/faks/master/vicon-pupil-dataset/21.8.2019/streamcpp/output/cizla_podignuto/output.csv',
    '/home/dnikic/Downloads/faks/master/vicon-pupil-dataset/mirkoTest/streamcpp/mirko_test/output.csv',
    '/home/dnikic/Downloads/faks/master/vicon-pupil-dataset/30.08.2019/streamcpp/30.08TM/podignuto/output.csv',
    # '/home/dnikic/Downloads/faks/master/vicon-pupil-dataset/30.08.2019/streamcpp/30.08TM/ponovljeno_srednje/output.csv',
    # '/home/dnikic/Downloads/faks/master/vicon-pupil-dataset/30.08.2019/streamcpp/30.08TM/spusteno/output.csv',
]


# find_csv('/home/dnikic/Downloads/faks/master/vicon-pupil-dataset')
# datapath = '/home/dnikic/Downloads/faks/master/vicon-pupil-dataset/21.8.2019/streamcpp/output/cizla_podignuto' + '/output.csv'
batch = 64
epochs = 1  # 150

for i in range(0,len(datapath)):
    print('\n','+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++')
    print(datapath[i])
    print('\n','+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++')
    print('\n Z2V1 \n')
    Z2V1.main(datapath[i], epochs, batch)
    print('\n Z3V1 \n')
    Z3V1.main(datapath[i], epochs, batch)


